# Premier React
Premier projet react, qui techniquement utilise Next.js, mais on s'en sert uniquement comme routeur, on voit surtout les bases de react qui reste valide même sans utiliser Next

## How To Use
1. Cloner le projet
2. Faire un `npm install`
3. Lancer le projet avec `npm run dev`et accéder au résultat sur http://localhost:3000


## Exercices
### Petit compteur
1. Créer une nouvelle variable count en utilisant le useState
2. Afficher cette variable dans le template en dessous du paragraphe par exemple
3. Créer une fonction increment qui va ajouter 1 au count, en utilisant le setCount
4. Rajouter un bouton dans le template qui, au click, va déclencher le increment
5. On fait pareil pour decrement 

### Nouvelle page avec condition 
1. Créer une nouvelle page/component structure.tsx avec sa fonction et tout dedans (et que ça return un main avec un h1 par exemple) [cette page sera accessible sur http://localhost:3000/structure]
2. Dans cette page, créer un state showPara qui va être un booléen initialisé à true
3. Créer un bouton qui, lorsqu'on cliquera dessus, passera la valeur de showPara de true à false ou inversement
4. Rajouter un paragraphe dans le template et via une condition faire qu'il s'affiche si showPara est true 

### Premier component : Faire un component ChangeName
Externaliser le state, la logique et le template d'affichage et de changement de nom du fichier index.tsx et le mettre dans un nouveau component ChangeName.tsx qu'on chargera dans l'index

### Lier un input à un state
1. Créer une nouvelle page form.tsx, et dans cette page mettre un input type text et un paragraphe
2. Créer un state message qui aura comme valeur par défaut une chaîne de caractère vide et afficher ce message dans le paragraphe dans le template
3. Chercher sur la doc comment faire pour lier un input html avec un state d'un component
4. Créer un bouton reset, qui au click remettra à zéro la valeur du message et donc la valeur de l'input
5. Rajouter à côté de l'input le nombre de caractères du message actuel
6. Créer une variable avec un nombre de caractères max et faire que si jamais le message dépasse ce nombre de caractère, l'affichage du nombre de caractère devienne rouge 

### Liste de person
1. Créer dans le src un fichier entities.ts dans lequel on déclare une interface Person qui aura un name:string et un firstName:string et un id:number (optionnel)
2. Créer une nouvelle page person-list.tsx dans laquelle on va créer un state persons qui sera comme type un tableau de Person (on peut indiquer le type du useState en le précisant avant les parenthèses genre useState<string>(''), bon dans le cas d'une string c'est inutile pasqu'il déduit déjà que c'est une string, mais pour un tableau c'est utile)
3. Faire une boucle pour afficher les personne dans le template
4. Rajouter un formulaire dans le template avec 2 input et un bouton, et faire que quand on valide ce formulaire ça rajoute un objet Person dans le tableau (plus d'étape en dessous pour celleux qui souhaitent)
5. Rajouter du bootstrap pour afficher les personnes sous forme de card ou chais pas quoi 

#### indices formulaire person :
* On va avoir besoin d'un state de type Person qui contiendra la nouvelle person avec des valeurs vides à ses propriétés, ou bien 2 state un pour le name et un pour le firstName 
* On devra lier, comme avec le message, les deux states ou bien les propriétés de la Person aux input de notre formulaire 
* On pourra rajouter un event au submit qui va commencer par un event.preventDefault() et qui va ensuite pusher la Person (ou créer la person si on a fait 2 states) dans la liste, de la même manière qu'on a pushé un name dans le structure.tsx
* On peut aussi faire en sorte d'avoir une variable qui contient l'id où on en est, pour l'assigner aux nouvelles personnes et l'incrémenter