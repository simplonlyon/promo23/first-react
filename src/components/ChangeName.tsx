import { useState } from "react";

interface Props {
    name:string;
}

export default function ChangeName({name}:Props) {

    const [maVariable, setMaVariable] = useState(name);

    function change() {
        setMaVariable('Autre chose');
    }

    return (
        <>
            <button onClick={change}>Click !</button>
            <p>Coucou {maVariable}</p>
        </>
    );
}