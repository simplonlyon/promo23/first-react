import { useState } from "react";

/**
 * Un component permet de contenir le template, les states et la logique liés à un fragment d'interface.
 * Il pourra être appelé depuis d'autres composants et on pourra donc construire nos pages à partir de composants.
 */
export default function Counter() {
    const [count, setCount] = useState(0);


    function increment() {
        setCount(count + 1);
    }

    function decrement() {
        setCount(count - 1);
    }

    return (
        <p>
            <button onClick={decrement}>-</button>
            {count}
            <button onClick={increment}>+</button>
            {/* On peut aussi utiliser directement une fat arrow si on a besoin de passer des argument ou qu'on ne fait qu'un set... */}
            {/* <button onClick={() => setCount(count+1)}>+</button> */}
        </p>
    )
}