import type { AppProps } from 'next/app'
import '../styles/global.css';



/**
 * Le fichier _app.tsx est un fichier réservé de Next qui sera chargé sur toutes les pages. Il permet
 * de charger des styles globaux (comme le global.css ici, ou bien le style de bootstrap par exemple)
 * ainsi que de définir la structure commune à toutes nos pages (on peut par exemple entouré le <Component /> par
 * un header et un footer, un menu de navigation ou autre)
 */
export default function MyApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}