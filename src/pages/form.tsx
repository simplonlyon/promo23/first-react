import { CSSProperties, useState } from "react";



export default function Form() {
    const [message, setMessage] = useState('Bonjour');
    const [max, setMax] = useState(10);

    function errorStyle():CSSProperties|undefined {
        if(message.length > 10) {
            return {
                color: 'red',
                fontStyle:'italic'
            }
        }
    }

    const errorClass = message.length > max ? 'error' : '';
    
    return (
        <>

            <input type="text" value={message} onChange={(event) => setMessage(event.target.value)} />
            <span className={errorClass}>{message.length} / {max}</span>

            <button onClick={() => setMessage('')}>Reset</button>
            <p style={errorStyle()}>{message}</p>     
        </>
    );
}