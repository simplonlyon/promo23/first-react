import ChangeName from "@/components/ChangeName";
import Counter from "@/components/Counter";


/**
 * Avec NextJS, les composants créés dans le dossier src/pages/ seront considérés comme des pages, c'est
 * à dire qu'on pourra accéder à ces component avec le router en tapant le nom du fichier dans l'url
 * (par exemple si j'ai un fichier src/pages/article.tsx je pourrais y accéder via http://localhost:3000/article)
 */
export default function Index() {


    return (
        <div>
            <ChangeName name="Jean" />
            <ChangeName name="Bloup" />
            <ChangeName name="Blop"  />
            <Counter />
        </div>
    );
}
