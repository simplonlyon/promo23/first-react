import { Person } from "@/entities";
import { FormEvent, useState } from "react";

/**
 * ce truc là c'est juste pour les test sans serveur ni rien, mais il sera dégagé à terme, donc peu importe
 * que ça soit un state ou pas
 */
let id = 4;
export default function PersonList() {
    /**
     * On déclare ici un state qui contiendra un tableau d'objet personne avec quelques personnes par défaut.
     * En réalité par la suite, ces personnes elles seront récupérer par un appel vers le backend et y'aura pas 
     * besoin de les déclarer en dur comme ça, mais ça peut être une idée si on veut déjà commencer à coder 
     * le react sans avoir le backend de fait
     */
    const [persons, setPersons] = useState<Person[]>([
        { id: 1, name: 'Name 1', firstName: 'First name 1' },
        { id: 2, name: 'Name 2', firstName: 'First name 2' },
        { id: 3, name: 'Name 3', firstName: 'First name 3' }
    ]);
    /**
     * On déclare en state un objet qui contiendra les valeurs de formulaire qui, ici, correspondent
     * directement à l'entité 
     */
    const [form, setForm] = useState<Person>({
        name: '',
        firstName: ''
    });
    /**
     * Une fonction qui pourra être réutilisé pour un peu n'importe quel formulaire, tant que les
     * input du formulaire dans le html ont un name qui correspond au nom des propriétés de l'objet form
     * En gros on change le state de l'objet form en lui donnant toutes les propriétés-valeurs qu'il contient
     * déjà et en modifiant spécifiquement la valeur (event.target.value) de la propriété dont le nom correspond au name de l'input (event.target.name)
     */
    function handleChange(event:any) {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }
    /**
     * Fonction permettant de rajouter une nouvelles personnes, en prenant les valeurs du formulaires, dans
     * le state des persons
     */
    function addPerson(event:FormEvent) {
        event.preventDefault();
        setPersons([
            ...persons,
            { id, ...form}
        ]);
        id++;
    }


    return (
        <>
            <form onSubmit={addPerson}>
                <label htmlFor="name">Name : </label>
                <input type="text" name="name" value={form.name} onChange={handleChange} />

                <label htmlFor="firstName">First Name : </label>
                <input type="text" name="firstName" value={form.firstName} onChange={handleChange}  />
                <button>Add Person</button>
            </form>
            <ul>
                {persons.map((item) =>
                    <li key={item.id}>{item.firstName} {item.name}</li>
                )}
            </ul>
        </>
    );
}