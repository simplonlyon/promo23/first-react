import { useState } from "react"



export default function Structure() {
    /**
     * Pour déclarer une variable dont la valeur changera au cours de la navigation, on utilise un
     * useState qui nous renvoie (grâce à la destructuration), en premier la variable qu'on pourra utilisée
     * dans le component/le template, et en second une fonction setter qui permettra de changer la valeur de
     * cette variable. Il est obligatoire de passer par cette fonction setter pour modifier une valeur,
     * même dans le cas d'un tableau ou d'un objet, car c'est l'appel à cette fonction qui indique à React
     * que l'affichage devra être recalculé
     */
    const [showPara, setShowPara] = useState(true);

    const [names, setNames] = useState(['Name 1', 'Name 3', 'Name 4']);

    function toggle() {
        setShowPara(!showPara);
    }
    
    function addName() {
        /**
         * Exemple d'ajout d'un nouvel élément dans un tableau en state, on ne fait pas de push,
         * mais on crée un nouveau tableau dans lequel on met les valeurs du tableau actuel via le 
         * spread operator (...), et on y ajoute la nouvelle valeur
         */
        setNames([...names, 'Name 5']);
    }

    // function loop() {
    //     let tab = [];
    //     for(const name of names) {
    //         tab.push(<p key={name}>{name}</p>);
    //     }
    //     return tab;
    // }
    
    return (
        <main>
            <h1>Condition & loop</h1>
            {/* Pour remplacer les addEventListener, on peut indiquer directement les noms des fonctions
            à appeler sur l'événement voulu directement dans le template */}
            <button onClick={addName}>Add name</button>

            {/* Pour faire une boucle, on utilise la fonction .map() des tableaux qui permet de créer un nouveau
            tableau dont les valeurs sont remplacés par ce qu'on return dans la fat arrow, ici, du JSX */}
            {names.map((item, index) =>
                <p key={index}>{item}</p>
            )}
            <button onClick={toggle}>Toggle para</button>
            {/* Pour faire un affichage conditionel, on peut utiliser l'opérateur logique && qui calculera
            ou non ce qu'il y a à sa droite selon si l'expression à gauche est true ou false */}
            {showPara &&
                <p>Displayed or not</p>
            }

            {/* On peut également utiliser un ternaire `condition ? <p>vrai</p>:<p>faux</p>` pour faire un
            if-else dans le template directement */}

        </main>
    )
}